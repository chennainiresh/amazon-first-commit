package beam;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Search_and_verify extends StartEngine{
	 
	
  @Test(priority = 1, description = "Search a product, Verify it and add the product to cart and verify the product and price in the cart" )
  public static void Add_Product_To_Cart_and_Verify_it() throws Exception
  {
	    
	  	WebElement searchBox = driver.findElement(By.id("twotabsearchtextbox"));
	  	searchBox.click();
	  	searchBox.sendKeys("Hard Disk");
	  	driver.findElement(By.xpath("//div[contains(@class,'nav-search-submit nav-sprite')]//input[1]")).click();
		String actuallist = driver.findElement(By.id("pdagDesktopSparkleDescription3")).getText();
		String expectedlist = "WD My Passport 4TB Portable External Hard Drive (Yellow)";
		Assert.assertEquals(actuallist, expectedlist);
		System.out.println("Product in the list is same " + actuallist);
		driver.findElement(By.id("pdagDesktopSparkleDescription3")).click();		  
		/*
		 * String currentActiveWindowRef = driver.getWindowHandle(); Set<String>
		 * allWindowRef = driver.getWindowHandles(); //for getting the unique reference
		 * for current session List<String> listRef = new ArrayList<String>();
		 * listRef.addAll(allWindowRef);
		 * System.out.println("Title before switch "+driver.getTitle());
		 * driver.switchTo().window(listRef.get(1));
		 * System.out.println("Title after switch "+driver.getTitle());
		 */
		  
		  driver.findElement(By.name("submit.add-to-cart")).click();
		  driver.findElement(By.id("hlb-view-cart-announce")).click();
		  String autualProductname = driver.findElement(By.xpath("//span[contains(@class,'a-size-medium sc-product-title')]")).getText();
		  String expectedProductname="WD My Passport 4TB Portable External Hard Drive (Yellow)";
		  Assert.assertEquals(autualProductname, expectedProductname);
		  System.out.println("Product in the cart is same as selected");
		  Thread.sleep(2000); String autualProductprice =driver.findElement(By.xpath("(//span[contains(@class,'a-size-medium a-color-price')])[3]")).getText();
		  String expectedProductprice = "  8,499.00";
		  Assert.assertEquals(autualProductprice, expectedProductprice);
		  System.out.println("Product Price in the cart is same as selcted");
		 
	   
	    
  }
  

 
}
