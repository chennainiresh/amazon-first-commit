package beam;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

public class StartEngine {
	public static ChromeDriver driver;
	

	@BeforeTest()
	  public static void launch_browser()
	  {
		 
		  	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		  	driver = new ChromeDriver();
		  	driver.get("https://www.amazon.in/");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			
	  }
	
@AfterTest()
public static void close_browser()
{
	 driver.quit();
		
	  }
}
